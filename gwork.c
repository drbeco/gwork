/***************************************************************************
 *   gwork.c                               Version 20220521.005612         *
 *                                                                         *
 *   Give yourself credits for your group work                             *
 *   Copyright (C) 2018-2023        by Ruben Carlo Benante                 *
 ***************************************************************************
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License.               *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
 *   To contact the author, please write to:                               *
 *   Ruben Carlo Benante                                                   *
 *   Email: rcb@beco.cc                                                    *
 *   Webpage: http://www.beco.cc                                           *
 *   Phone: +55 (81) 3184-7555                                             *
 ***************************************************************************/

/* ---------------------------------------------------------------------- */
/**
 * @file gwork.c
 * @ingroup GroupUnique
 * @brief Give yourself credits for your group work
 * @details This program really do a nice job as a template, and template only!
 * @version 20210722.195722
 * @date 2018-05-16
 * @author Ruben Carlo Benante <<rcb@beco.cc>>
 * @par Webpage
 * <<a href="http://www.beco.cc">www.beco.cc</a>>
 * @copyright (c) 2018 GNU GPL v2
 * @note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>.
 *
 *
 * @todo Now that you have the template, hands on! Programme!
 * @warning Be carefull not to lose your mind in small things.
 * @bug This file right now does nothing usefull
 *
 */

/*
 * Instrucoes para compilar:
 *   preferencialmente inclua um makefile e use:
 *   $ make
 *
 *   ou
 *
 *   $gcc gwork.c -o gwork.x -Wall
 *        -Wextra -ansi -pedantic-errors -g -O0 -DDEBUG=1 -DVERSION="0.1.160612.142044"
 *
 * Modelo de indentacao:
 * Estilo: --style=allman ou -A1
 *
 * Opcoes: -A1 -s4 -k3 -xj -SCNeUpv
 *
 *  * No vi use:
 *      :% !astyle -A1 -s4 -k3 -xj -SCNeUpv
 *  * No linux:
 *      $astlye -A1 -s4 -k3 -xj -SCNeUpv gwork.c
 */

/* ---------------------------------------------------------------------- */
/* includes */

#include <stdio.h> /* Standard I/O functions (already included by ncurses) */
#include <stdlib.h> /* Miscellaneous functions (rand, malloc, srand)*/
#include <getopt.h> /* get options from system argc/argv */
#include <ncurses.h> /* Screen handling and optimisation functions */
#include <string.h> /* Strings functions definitions */
#include <math.h> /* Mathematics functions */
#include <pwd.h> /* unix groups and users */
#include <unistd.h> /* UNIX standard function */
#include <sys/types.h> /* unix getuid */
#include <ctype.h> /* Character functions */
#include <sys/stat.h> /* File status and information */
/* include assert.h after #define DEBUG */

/* ---------------------------------------------------------------------- */
/* definitions */

/* #define _GNU_SOURCE */
/* ignore -Wno-unused-result */
#define igr(M) if(1==((long)M)){;}

#ifndef VERSION /* gcc -DVERSION="0.1.160612.142628" */
#define VERSION "20210722.195722" /**< Version Number (string) */
#endif

/* Debug */
#ifndef DEBUG /* gcc -DDEBUG=1 */
#define DEBUG 0 /**< Activate/deactivate debug mode */
#endif

#if DEBUG==0
#define NDEBUG
#endif
#include <assert.h> /* Verify assumptions with assert. Turn off with #define NDEBUG */

/** @brief Debug message if DEBUG on */
#define IFDEBUG(M) if(DEBUG) fprintf(stderr, "[DEBUG file:%s line:%d]: " M "\n", __FILE__, __LINE__); else {;}

/* limits */
#define BIGLINE 500 /**< String buffer */
#define SBUFF 150
#define SBUF2 200
#define SMALL 5
#define NWIN 3 /* Number of windows */
#define CENTER 1 /* centered justified */

#define COINS 72 /* coins in the bank to be used by all students */
#define MAXG 25 /* max students in a group */
#define LASTHABIL 6 /* number of habilities */
#define COORD -2 /* coordinator running the program */

/* message status line */
#define STOK 1
#define STMSG 2
#define STYESNO 3

/* #define SLINE "--------------------------------------------------------------------------------" */

/* ---------------------------------------------------------------------- */
/* types */

typedef struct sdata
{
    /* group data */
    int ni; /* number of students in this group */
    int si; /* the current student index */
    int cd; /* true/false, coordinator running software */
    char name[MAXG][SBUFF]; /* students names */
    char coord[SBUFF]; /* coordinator's name */
    char title[SBUFF]; /* title of the project */

    /* menu data */
    int change; /* is data changed */
    int ucoins; /* balance of unused coins */
    int coins[MAXG]; /* coins for each member */
    int habil[MAXG][LASTHABIL]; /* values for each hability */
    int bonus[LASTHABIL]; /* bonus for each hability */
    int mpes; /* selected item on menu pessoa */
    int mhab; /* selected item on menu habils */
    int themenu; /* menu 0 = names, menu 1 = habils */
} data_t; /* selection window */

typedef struct tswin
{
    int lmax, cmax; /* size of window */
    int wi; /* current window */
    WINDOW *w[NWIN]; /* some windows */
} wins_t;

/* ---------------------------------------------------------------------- */
/* globals */

const char *shabil[LASTHABIL]={"projeto", "pesquisa", "conteudo", "desenvolve", "revisao", "validacao"};
const char *swin[NWIN]={"Help", "Selection", "Copyright"};

int fwclear0, fwclear1, fwclear2; /* clear windows */

/* ---------------------------------------------------------------------- */
/* prototypes */

int msgstatus(wins_t win, char *msg, int opt); /* ncurses status line msg */
void bomb(wins_t w, char *msg, FILE *f); /* atexit function */

data_t gwork_init(wins_t win); /* global initialization function */
int price(int hv[LASTHABIL]); /* return price of set */
void set_bonus(int hab[LASTHABIL], int bonus[LASTHABIL]); /* the the hability modifier */
void help(void); /* print some help */
void copyr(void); /* print version and copyright information */
void printwin(wins_t win, data_t dt); /* print current window */
void printwin1(wins_t win, data_t dt); /* print selection window */
void printwin0(wins_t win, data_t dt); /* window zero : help */
void printwin2(wins_t win, data_t dt); /* window two : license and copyright */
int itsme(char *name); /* return true if the name is current user */
int groupcmp(data_t ga, data_t gb); /* compare two group_t structures */
void printst(data_t d); /* print struct */
data_t addcoin(data_t dt); /* add coins to user */
data_t subcoin(data_t dt); /* sub coins from user */
data_t submenu(data_t dt); /* diminish menu item */
data_t addmenu(data_t dt); /* advance menu item */
int ccenter(char const * const msg, int offset); /* return the column to print a centered msg */
void saveconf(wins_t win, data_t dt); /* creates gwork.conf */
float grading(data_t dt); /* calculates the grading system */
float abscissa(int co, int tn); /* return normal 0..1 for grading */

/* ---------------------------------------------------------------------- */
/**
 * @ingroup GroupUnique
 * @brief This is the main event of the evening
 * @details Main function
 * @return int
 * @author Ruben Carlo Benante
 * @version 20210722.195722
 * @date 2021-07-29
 *
 */
int main(int argc, char *argv[])
{
    int opt; /* return from getopt() */
    int kpress; /* the key pressed */

    /* WINDOW *w[NWIN]; */
    wins_t win; /* a set of windows and its geometry */
    data_t dt; /* "selection" window info, with group and menu */

    IFDEBUG("main()");

    /* getopt() configured options:
     *        -h  help
     *        -V  version
     */
    opterr = 0;
    while((opt = getopt(argc, argv, "hV")) != EOF)
        switch(opt)
        {
            case 'h':
                help();
                break;
            case 'V':
                copyr();
                break;
            case '?':
            default:
                fprintf(stderr, "Type\n\t$man %s\nor\n\t$%s -h\nfor help.\n\n", argv[0], argv[0]);
                IFDEBUG("main() end");
                return EXIT_FAILURE;
        }

    win.wi=-1;
    dt=gwork_init(win); /* initialization function */
    /* printst(dt); */

    slk_init(CENTER); /* soft labels, centered */
    initscr(); /* initializes screen */

    slk_set(1, "F1 Help", CENTER); /* ? */
    slk_set(2, "F2 Load", CENTER); /* d */
    slk_set(3, "F3 Save", CENTER); /* w */
    slk_set(4, "F4 Quit", CENTER); /* q */
    slk_set(5, "F5 Next", CENTER); /* PgUp */
    slk_set(6, "F6 Prev", CENTER); /* PgDn */
    slk_set(7, "F7 Unkn", CENTER);
    slk_set(8, "F8 Unkn", CENTER);

    slk_refresh();
    for(win.wi=0; win.wi<NWIN; win.wi++)
        if((win.w[win.wi]=newwin(0,0,0,0))==NULL)
        {
            IFDEBUG("main() end");
            fprintf(stderr, "unable to create window %d\n", win.wi);
            for(--win.wi; win.wi>=0; win.wi--)
                delwin(win.w[win.wi]);
            endwin();
            exit(1);
        }
        else
            box(win.w[win.wi], 0, 0);
    win.wi=1; /* select */

    refresh(); /* clear for the first time the standard screen and start the program */

    noecho(); /* getch don't echo */
    curs_set(0); /* no cursor showing */
    nodelay(stdscr, TRUE); /* getch don't block */
    keypad(stdscr, TRUE); /* can read function keys F1..F12 */
    halfdelay(2); /* similar to cbreak() wait for one tenth of a second and move on */

    /* mvwprintw(win.w[win.wi], 1, (COLS-25)/2, "Test NCURSES version %s (size: l=%d, c=%d)\n", NCURSES_VERSION, LINES, COLS); */

    win.lmax=LINES;
    win.cmax=COLS;

    printwin(win, dt);
    do
    {
        kpress=getch();
        /* flushinp(); */

        /* menu move or value move */
        if(kpress == KEY_RIGHT || 'l' == kpress)
        {
            if(dt.themenu)
                dt=addmenu(dt);
            else
                dt=addcoin(dt);
        }
        /* menu move or value move */
        if(kpress == KEY_LEFT || 'h' == kpress)
        {
            if(dt.themenu)
                dt=submenu(dt);
            else
                dt=subcoin(dt);
        }
        /* menu move or value move */
        if(kpress == KEY_UP || 'k' == kpress)
        {
            if(dt.themenu)
                dt=addcoin(dt);
            else
                dt=submenu(dt);
        }
        /* menu move or value move */
        if(kpress == KEY_DOWN || 'j' == kpress)
        {
            if(dt.themenu)
                dt=subcoin(dt);
            else
                dt=addmenu(dt);
        }
        /* change menu */
        if(kpress == '\t' || ' ' == kpress)
        {
            if(dt.cd==COORD) /* coordinator */
            {
                msgstatus(win, "Coordinator mode.", STMSG);
                dt.si=dt.mpes; /* Coordinator picked a student to change */
                /* continue; */
            }
            if(dt.cd!=COORD && dt.mpes!=dt.si && dt.themenu==0)
            {
                msgstatus(win, "You can only change your own values.", STOK);
                continue;
            }
            dt.themenu=(dt.themenu+1)%2;
            /* if(dt.themenu) */
                /* dt.mitem=0; */
            /* else */
                /* dt.mitem=dt.si; */
        }
        /* PgUp ou F5 Next */
        if(kpress == KEY_NPAGE || KEY_F(5)==kpress)
            win.wi = (win.wi+1)%NWIN;
        /* PgDn ou F6 Prev */
        if(kpress == KEY_PPAGE || KEY_F(6)==kpress)
            win.wi = win.wi?win.wi-1:NWIN-1;
        /* ? ou F1 Help */
        if(KEY_F(1)==kpress || '?' == kpress || '/' == kpress)
            win.wi = win.wi?0:1;
        /* o ou F2 Load - discard changes */
        if(KEY_F(2)==kpress || 'o' == kpress)
        {
            if(dt.change)
            {
                if(msgstatus(win, "Are you sure you want to discard your changes and read values from disk?", STYESNO))
                    dt=gwork_init(win); /* initialization function */
            }
            else
                msgstatus(win, "No changes, no reading.", STOK);
                /* if(msgstatus(win, "Are you sure you want to read values from disk?", STYESNO)) */
                /*     dt=gwork_init(); /1* initialization function *1/ */
        }
        /* s ou F3 Save */
        if(KEY_F(3)==kpress || 'w' == kpress)
        {
            if(dt.change)
            {
                if(msgstatus(win, "Are you sure you want to save your changes to disk?", STYESNO))
                {
                    saveconf(win, dt); /* creates gwork.conf */
                    dt.change=0;
                }
            }
            else
                msgstatus(win, "No changes, no saving.", STOK);
        }
        /* q ou F4 Quit */
        if(KEY_F(4)==kpress || 'q' == kpress)
        {
            if(dt.change)
            {
                if(msgstatus(win, "Are you sure you want discard changes and quit?", STYESNO))
                    kpress='Q';
            }
            else
                if(msgstatus(win, "Are you sure you want to quit?", STYESNO))
                    kpress='Q';
        }
        printwin(win, dt);
        napms(30); /* don't do a busy loop */
    /* fprintf(stderr, "keypress=%c\n", kpress); */
    }while(kpress != 'Q');

    /* fprintf(stderr, "saiu\n"); */

    bomb(win, "", NULL);

    /* refresh(); */
    /* getch();  */
    /* sleep(1); */
    /* endwin(); /1* properly close ncurses *1/ */
    /* sleep(1); */
    /* IFDEBUG("main() end"); */
    /* exit(0); /1* the end *1/ */
    /* abort(); */
    /* return EXIT_SUCCESS; */
}

/* ---------------------------------------------------------------------- */
void bomb(wins_t win, char *msg, FILE *f)
{
    IFDEBUG("bomb");
    int i;

    refresh();

    if(win.wi!=-1)
        for(i=0; i<NWIN; i++)
            delwin(win.w[i]);

    endwin();

    fprintf(stderr, "%s", msg);

    if(f)
        fclose(f);
    exit(1);
}

/* ---------------------------------------------------------------------- */
/* return the column to print a centered msg */
int ccenter(char const * const msg, int of)
{
    IFDEBUG("ccenter");
    return (COLS - strlen(msg) - of)/2;
}

/* ---------------------------------------------------------------------- */
/* add coins to user */
data_t addcoin(data_t dt)
{
    IFDEBUG("addcoin");
    int oldval;
    dt.change=1;
    if(dt.themenu) /* add +1 to a single habil */
    {
        oldval=dt.habil[dt.si][dt.mhab];
        dt.habil[dt.si][dt.mhab]++; /* increase habil */
        if(price(dt.habil[dt.si])>dt.coins[dt.si])
            dt.habil[dt.si][dt.mhab]=oldval;
    }
    else /* add coin to a person */
        if(dt.ucoins>0) /*dt.coins[dt.mitem]<dt.0)*/
        {
            dt.coins[dt.mpes]++;
            dt.ucoins--;
        }
    return dt;
}

/* ---------------------------------------------------------------------- */
/* sub coins from user */
/* do not allow -- if used coins by user or by others */
data_t subcoin(data_t dt)
{
    IFDEBUG("subcoin");
    dt.change=1;
    if(dt.themenu) /* sub -1 from a single habil */
    {
        dt.habil[dt.si][dt.mhab] = dt.habil[dt.si][dt.mhab]>0?dt.habil[dt.si][dt.mhab]-1:dt.habil[dt.si][dt.mhab];
        return dt;
    }

    /* sub coin from a person */
    if(dt.coins[dt.mpes]<=0)
        return dt;

    /* se a moeda que tenho ja esta empenhada em habilidades, nao diminui */
    if(dt.coins[dt.mpes]-price(dt.habil[dt.mpes])<=0)
        return dt;

    dt.coins[dt.mpes]--;
    dt.ucoins++;
    return dt;
}

/* ---------------------------------------------------------------------- */
/* diminish menu item */
data_t submenu(data_t dt)
{
    IFDEBUG("submenu");
    if(dt.themenu)
        dt.mhab = dt.mhab?dt.mhab-1:LASTHABIL-1;
    else
        dt.mpes = dt.mpes?dt.mpes-1:dt.ni-1;
    return dt;
}

/* ---------------------------------------------------------------------- */
/* advance menu item */
data_t addmenu(data_t dt)
{
    IFDEBUG("addmenu");
    if(dt.themenu)
        dt.mhab = (dt.mhab+1)%LASTHABIL;
    else
        dt.mpes = (dt.mpes+1)%dt.ni;
    return dt;
}

/* ---------------------------------------------------------------------- */
/* ncurses status line msg: STOK, STMSG, STYESNO */
int msgstatus(wins_t win, char *msg, int opt)
{
    IFDEBUG("msgstatus");
    char const * const lc="| Status line:";
    int l=LINES-3;
    int len;
    int item=1;
    int kpress;

    mvwhline(win.w[1], l++, 1, '-', COLS-2); /* horizontal line */
    len=strlen(lc)+strlen(msg)+7;
    if(opt==STYESNO)
    {
        mvwprintw(win.w[1], l, 4, "%s %s", lc, msg);
        do
        {
            if(item==1) /* YES */
            {
                wattrset(win.w[1], A_REVERSE);
                mvwprintw(win.w[1], l, len, "YEP");
                wattrset(win.w[1], A_NORMAL);
                mvwprintw(win.w[1], l, len+4, "NOT");
            }
            else /* NO */
            {
                wattrset(win.w[1], A_NORMAL);
                mvwprintw(win.w[1], l, len, "YEP");
                wattrset(win.w[1], A_REVERSE);
                mvwprintw(win.w[1], l, len+4, "NOT");
            }
            kpress=getch();
            if(kpress == KEY_RIGHT || 'l' == kpress || kpress == KEY_LEFT || 'h' == kpress)
                item*=-1;

            wattrset(win.w[1], A_NORMAL);
            wrefresh(win.w[1]);
            napms(30);
        }while(kpress != '\n');
        if(item==1)
            return 1; /* true YES */
        return 0; /* false = NO */
    }

    if(opt==STOK)
    {
        mvwprintw(win.w[1], l, 4, "%s %s", lc, msg);
        wattrset(win.w[1], A_REVERSE);
        mvwprintw(win.w[1], l, len, "OKEY");
        wattrset(win.w[1], A_NORMAL);
        wrefresh(win.w[1]);
        nodelay(win.w[1], FALSE);
        do
        {
            kpress=getch();
            napms(30);
        }while(kpress != '\n');
        nodelay(win.w[1], TRUE);
        return 1; /* true = OK */
    }

    if(opt==STMSG)
    {
        mvwprintw(win.w[1], l, 4, "%s %s", lc, msg);
        wrefresh(win.w[1]);
        nodelay(win.w[1], FALSE);
        napms(500); /* wait a bit just to sync eyes */
        nodelay(win.w[1], TRUE);
        return 1;
    }
    return 0; /* this should never return */
}

/* ---------------------------------------------------------------------- */
/* print window select */
void printwin(wins_t win, data_t dt)
{
    IFDEBUG("printwin");
    switch(win.wi)
    {
        case 0: /* help */
        default:
            printwin0(win, dt);
            break;
        case 1: /* habilities */
            printwin1(win, dt);
            break;
        case 2: /* license and copyright */
            printwin2(win, dt);
            break;
    }
}

/* ---------------------------------------------------------------------- */
/* window zero : help */
void printwin0(wins_t win, data_t dt)
{
    IFDEBUG("printwin0");
    int l=1;
    char const * const lc="Help";

    if(fwclear0)
    {
        wclear(win.w[0]);
        fwclear0=0;
        fwclear1=fwclear2=1;
    }

    mvwprintw(win.w[0], l++, ccenter(lc, 0), "%s", lc);
    mvwhline(win.w[0], l++, 1, '-', COLS-2); /* horizontal line */
    mvwprintw(win.w[0], l++, ccenter(dt.coord, 13), "Coordinator: %s", dt.coord);
    mvwhline(win.w[0], l++, 1, '-', COLS-2); /* horizontal line */

    l++;
    mvwprintw(win.w[0], l++, 2, "== Teclas de Função ==");
    mvwprintw(win.w[0], l++, 2, "<F1> ou <?>    : para esta tela de ajuda.");
    mvwprintw(win.w[0], l++, 2, "<F2> ou <o>    : para descartar suas mudanças.");
    mvwprintw(win.w[0], l++, 2, "<F3> ou <w>    : para salvar suas mudanças.");
    mvwprintw(win.w[0], l++, 2, "<F4> ou <q>    : para sair do programa após confirmação.");
    mvwprintw(win.w[0], l++, 2, "<Q>            : para sair do programa imediatamente.");
    mvwprintw(win.w[0], l++, 2, "<F5> ou <PgUp> : para avancar pelas telas disponíveis.");
    mvwprintw(win.w[0], l++, 2, "<F6> ou <PgDn> : para retroceder pelas telas disponíveis.");
    mvwprintw(win.w[0], l++, 2, "<F7>  e <F8>   : reservadas para uso futuro.");
    l++;
    mvwprintw(win.w[0], l++, 2, "== Tela de Seleção ==");
    mvwprintw(win.w[0], l++, 2, "<TAB> ou <SP>  : alterna entre menu de alunos e menu de tópicos");
    l++;
    mvwprintw(win.w[0], l++, 2, ":: Menu de Alunos ::");
    mvwprintw(win.w[0], l++, 2, "<UP>    ou <k> : sobe para o nome anterior.");
    mvwprintw(win.w[0], l++, 2, "<DOWN>  ou <j> : desce para o nome seguinte.");
    mvwprintw(win.w[0], l++, 2, "<RIGHT> ou <l> : aumenta o saldo do nome atual.");
    mvwprintw(win.w[0], l++, 2, "<LEFT>  ou <h> : devolve moeda para o saldo conjunto.");
    l++;
    mvwprintw(win.w[0], l++, 2, ":: Menu de Tópicos ::");
    mvwprintw(win.w[0], l++, 2, "<UP>    ou <k> : aumenta o seu tempo dedicado ao tópico atual.");
    mvwprintw(win.w[0], l++, 2, "<DOWN>  ou <j> : diminui o seu tempo dedicado ao tópico atual.");
    mvwprintw(win.w[0], l++, 2, "<RIGHT> ou <l> : avança para o tópico seguinte.");
    mvwprintw(win.w[0], l++, 2, "<LEFT>  ou <h> : volta para o tópico anterior.");
    l++;
    mvwprintw(win.w[0], l++, 2, "== Controle de Versão ==");
    mvwprintw(win.w[0], l++, 2, ":: Adicionando o arquivo de configuração ::");
    mvwprintw(win.w[0], l++, 2, "* Na primeira vez que criar o arquivo gwork.conf, faça:");
    mvwprintw(win.w[0], l++, 2, "             $ git add gwork.conf");
    mvwprintw(win.w[0], l++, 2, ":: Registrando as alterações ::");
    mvwprintw(win.w[0], l++, 2, "* Sempre que modificar os valores dos tópicos ou moedas, execute o commit:");
    mvwprintw(win.w[0], l++, 2, "             $ git cm \"gwork: Nome Completo\"");
    mvwprintw(win.w[0], l++, 2, ":: Enviando as alterações ::");
    mvwprintw(win.w[0], l++, 2, "* Sempre que fizer commit, envie suas modificações para o remoto:");
    mvwprintw(win.w[0], l++, 2, "             $ git push");

    box(win.w[0], 0, 0);
    wrefresh(win.w[0]); /* print the screen */
    napms(30);
    /* wclear(win.w[0]); */
}

/* ---------------------------------------------------------------------- */
/* window two : license and copyright */
void printwin2(wins_t win, data_t dt)
{
    IFDEBUG("printwin2");
    int l=1;
    char const * const lc="License and Copyright";
    char sversion[SBUFF];

    if(fwclear2)
    {
        wclear(win.w[2]);
        fwclear2=0;
        fwclear1=fwclear0=1;
    }

    mvwprintw(win.w[2], l++, ccenter(lc, 0), "%s", lc);
    mvwhline(win.w[2], l++, 1, '-', COLS-2); /* horizontal line */
    mvwprintw(win.w[2], l++, ccenter(dt.coord, 13), "Coordinator: %s", dt.coord);
    mvwhline(win.w[2], l++, 1, '-', COLS-2); /* horizontal line */

    l++;
    mvwprintw(win.w[2], l++, ccenter("", 75), "/***************************************************************************");
    sprintf(sversion," *   gwork                                 Version %s       *", VERSION);
    mvwprintw(win.w[2], l++, ccenter("", 75), "%s", sversion);

    mvwprintw(win.w[2], l++, ccenter("", 75), " *                                                                         *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   Give yourself credits for your group work                             *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   Copyright (C) 2021+        by Ruben Carlo Benante                     *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " ***************************************************************************");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   This program is free software; you can redistribute it and/or modify  *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   it under the terms of the GNU General Public License as published by  *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   the Free Software Foundation; version 2 of the License.               *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *                                                                         *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   This program is distributed in the hope that it will be useful,       *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   GNU General Public License for more details.                          *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *                                                                         *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   You should have received a copy of the GNU General Public License     *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   along with this program; if not, write to the                         *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   Free Software Foundation, Inc.,                                       *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " ***************************************************************************");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   To contact the author, please write to:                               *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   Ruben Carlo Benante                                                   *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   Email: rcb@beco.cc                                                    *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   Webpage: http://www.beco.cc                                           *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " *   Phone: +55 (81) 3184-7555                                             *");
    mvwprintw(win.w[2], l++, ccenter("", 75), " ***************************************************************************/");

    box(win.w[2], 0, 0);
    wrefresh(win.w[2]); /* print the screen */
    napms(30);
}

/* ---------------------------------------------------------------------- */
/* selection window */
void printwin1(wins_t win, data_t dt)
{
    IFDEBUG("printwin1");
    int c=2, l=1, h;
    int ol=7; /* first line for name */
    int mybalance, chor, tbon; /* balance in coins, proportional carga-horaria, total bonus */
    float grad, scr; /* score proportional 0.0 - 100.0 */
    int tcoin, maxcoin; /* total coins spent, who spent the most */
    char const * const lc="  | Status line:";

    if(fwclear1)
    {
        wclear(win.w[1]);
        fwclear1=0;
        fwclear2=fwclear0=1;
    }

    /* ---------------------------------------------------------------------- */
    /* print themenu 0: AUTHORS */
    /* dt.mitem = [=0...<ni = nomes,    =ni...<ni+6 = habils]*/
    mvwprintw(win.w[1], l++, ccenter(dt.title, 7), "Title: %s", dt.title);
    mvwhline(win.w[1], l++, 1, '-', COLS-2); /* horizontal line */
    mvwprintw(win.w[1], l++, ccenter(dt.coord, 13), "Coordinator: %s", dt.coord);
    mvwhline(win.w[1], l++, 1, '-', COLS-2); /* horizontal line */
    l++;
    mvwprintw(win.w[1], l++, 1, "Coins | Students' Names");
    mvwprintw(win.w[1], l++, 1,"----- | ------------------");
    ol=l;

    tcoin=0;
    maxcoin=dt.coins[0];
    for(l=0; l<dt.ni; l++)
    {
        if(dt.mpes==l && dt.themenu==0)
            wattrset(win.w[1], A_REVERSE); /* attrset(A_BOLD); */
        mvwprintw(win.w[1], l +ol, c, "%3d  | %s", dt.coins[l], dt.name[l]); /* coin:name*/
        wattrset(win.w[1], A_NORMAL);
        tcoin+=dt.coins[l];
        if(maxcoin<dt.coins[l])
            maxcoin=dt.coins[l];
    }
    mvwprintw(win.w[1], l++ +ol, 1, "----- | ------------------");
    mvwprintw(win.w[1], l++ +ol, c, "%3d  | %s", dt.ucoins, "Available balance");

    l++; /* blank line */
    mvwhline(win.w[1], l +ol, 1, '-', COLS-2); /* horizontal line */
    l++;

    /* ---------------------------------------------------------------------- */
    /* print themenu 1: skills */
    set_bonus(dt.habil[dt.mpes], dt.bonus);
    tbon=0;
    for(h=0; h<LASTHABIL; h++)
    {
        if(dt.mhab==h && dt.themenu==1)
            wattrset(win.w[1], A_REVERSE); /* attrset(A_BOLD); */
        mvwprintw(win.w[1], l +ol+1, c, "%s", shabil[h]); /* label */
        wattrset(win.w[1], A_NORMAL);
        mvwprintw(win.w[1], l +ol+2, c, "%3d", dt.habil[dt.mpes][h]); /* habil value */
        mvwprintw(win.w[1], l +ol+3, c, "%3d", dt.bonus[h]); /* bonus value */
        tbon+=dt.bonus[h];
        c+=strlen(shabil[h])+1;
    }

    mybalance = dt.coins[dt.mpes]-price(dt.habil[dt.mpes]);
    /* chor = ((float)dt.coins[dt.si]/(float)tcoin)*72.0; */
    /* chor = (maxcoin*72)/dt.coins[dt.si]; */
    /* grad = ((float)(chor+tbon)/90.0)*100.0; */

    grad = grading(dt);
    chor = 72.0*grad/100.0;
    scr = grad*0.82+tbon;
    scr = scr<0.0?0.0:scr>100.0?100.0:scr;

    mvwprintw(win.w[1], l +ol+1, c, "| coins | grades");
    mvwprintw(win.w[1], l +ol+2, c, "|  %3d  |  %3d.0 C.H.     ", mybalance, chor);
    mvwprintw(win.w[1], l +ol+3, c, "|  %3d  |  %5.1f SCR.     ", tbon, scr);

    mvwhline(win.w[1], LINES-3, 1, '-', COLS-2); /* horizontal line */
    mvwprintw(win.w[1], LINES-2, 2, "%s ", lc); /* status line */
    wclrtoeol(win.w[1]);
    box(win.w[1], 0, 0);
    if(dt.change)
        mvwaddch(win.w[1], LINES-2, 2, '+');

    /* mvwprintw(win.w[1], LINES-5, 5, "DEBUG l=%d c=%d L=%d C=%d", win.lmax, win.cmax, LINES, COLS); */

    wrefresh(win.w[1]); /* print the screen */

    if(win.lmax!=LINES || win.cmax!=COLS)
    {
        napms(50);
        wclear(win.w[1]);
        win.lmax=LINES;
        win.cmax=COLS;
        printwin1(win, dt); /* recursive to touchwin */
    }
    else
        touchwin(win.w[1]);
}

/* ---------------------------------------------------------------------- */
/* return 0.0 ... 1.0 for grading */
float abscissa(int co, int tn)
{
    IFDEBUG("abscissa");
    float o, m, x;
    float p0, p1; /* proportions */

    if(tn==0)
        return 0.0;

    o = co/(float)COINS; /* what have i done */
    m = 1.0/(float)tn; /* average */

    if(m<0.00001)
        return 0.0;
    if(m>0.99999)
        return o;

    /* [0..o..m..1]=[0..x..1/2..1]; [0..m..o..1]=[0..1/2..x..1] */
    p0 = (0.0 - 0.5)/(0.0 - m); /* (min2 - max2)/(min1 - max1) */
    p1 = (0.5 - 1.0)/(m - 1.0); /* (min2 - max2)/(min1 - max1) */
    if(o <= m) /* [0..o..m]=[0..x..1/2] */
        x = o*p0 + 0.5 - m*p0;
    else /* [m..o..1]=[1/2..x..1] */
        x = o*p1 + 1.0 - 1.0*p1;

    if(x>1.0)
        x=1.0;
    if(x<0.0)
        x=0.0;
    return x;
}

/* ---------------------------------------------------------------------- */
/* calculates the grading system */
float grading(data_t dt)
{
    IFDEBUG("grading");
    int n; /* index for students on ni */
    int tn; /* total of working students */
    float g; /* grade score 0..100 */
    float x; /* the x-axis abicissa normalized value from 0..1/2..1 */

    tn=0;
    for(n=0; n<dt.ni; n++)
        if(dt.coins[n]>0)
            tn++;
    x=abscissa(dt.coins[dt.mpes], tn);
    /* g = 276.959*x*x*x - 600.797*x*x + 423.675*x - 0.162689; */
    /* g = 337.349*x*x*x - 719.8*x*x + 476.364*x - 0.356275; */
    g = 337.8*x*x*x - 715.8*x*x + 478.364*x - 0.356275;

    if(x<0.5)
        g = x*200.0;
    if(x>=0.5 && x<0.75)
        g = 110.0 - 20.0*x;
    if(x>=0.75)
        g = 20.0*x+80.0;

    if(g>99.0)
        g=100.0;
    if(g<0.0)
        g=0.0;
    return g;


/* linear
    ^
    │
100 - - - -••••- - - - - -••
    │     • |  ••       •• |
    │    •  |    ••   ••   |
 95 -   •   |      •••     |
    │  •    |       |      |
    │ •     |       |      |
    │•      |       |      |
  0 •───────+───────+──────+───>
    0       0.5     0.75   1.0

 wolfram alpha: fit {{0,0},{0.5,100},{0.75,95},{1,100},{0.4,88}}
    ^
    │
100 - - -••••••••••- - - - - - ••
    │  •••   |    ••         •••|
    │ ••     |     ••••   ••••  |
 95 - •      |        •••••     |
    │••      |          |       |
    │•       |          |       |
    │•       |          |       |
  0 •────────+──────────+───────+───>
    0       0.5        0.75    1.0
*/
}

/* ---------------------------------------------------------------------- */
/* the the hability modifier */
void set_bonus(int hv[LASTHABIL], int bon[LASTHABIL])
{
    IFDEBUG("set_bonus");
    int h;

    for(h=0; h<LASTHABIL; h++)
    {
        bon[h] = (int)floor((hv[h]-10)/2.0);
        /* printw("bon[%d]=%d, ", h, bon[h]); */
    }
    IFDEBUG("set_bonus end");
}

/* ---------------------------------------------------------------------- */
/* return price of habil set */
int price(int hv[LASTHABIL])
{
    IFDEBUG("price");
    int h, sp=0;
    int cost[8]={0, 1, 2, 3, 4, 5, 7, 9};

    for(h=0; h<LASTHABIL; h++)
    {
        if(hv[h]<8)
            continue;
        if(hv[h]>15)
        {
            sp+=((hv[h]-12)*3);
            continue;
        }
        sp+=cost[hv[h]-8];
    }
    return sp;
}

/* ---------------------------------------------------------------------- */
/**
 * @ingroup GroupUnique
 * @brief Prints help information and exit
 * @details Prints help information (usually called by opt -h)
 * @return Void
 * @author Ruben Carlo Benante
 * @version 20210722.195722
 * @date 2018-05-16
 *
 */
void help(void)
{
    IFDEBUG("help()");
    printf("%s - %s\n", "gwork", "Give yourself credits for your group work.\n");
    printf("\nUsage: %s [-h|-v]\n", "gwork");
    printf("\nOptions:\n");
    printf("\t-h,  --help\n\t\tShow this help.\n");
    printf("\t-V,  --version\n\t\tShow version and copyright information.\n");
    printf("\t-v,  --verbose\n\t\tSet verbose level (cumulative).\n");
    printf("\n");

    printf("------------------------------------------------------------------------------\n");
    printf("\n");

    printf("== Teclas de Função ==\n");
    printf("<F1> ou <?>    : para esta tela de ajuda.\n");
    printf("<F2> ou <o>    : para descartar suas mudanças.\n");
    printf("<F3> ou <w>    : para salvar suas mudanças.\n");
    printf("<F4> ou <q>    : para sair do programa após confirmação.\n");
    printf("<Q>            : para sair do programa imediatamente.\n");
    printf("<F5> ou <PgUp> : para avancar pelas telas disponíveis.\n");
    printf("<F6> ou <PgDn> : para retroceder pelas telas disponíveis.\n");
    printf("<F7> e  <F8>   : reservadas para uso futuro.\n");
    printf("\n");

    printf("== Tela de Seleção ==\n");
    printf("<TAB> ou <SP>  : alterna entre menu de alunos e menu de tópicos\n");
    printf("\n");
    printf(":: Menu de Alunos ::\n");
    printf("<UP>    ou <k> : sobe para o nome anterior.\n");
    printf("<DOWN>  ou <j> : desce para o nome seguinte.\n");
    printf("<RIGHT> ou <l> : aumenta o saldo do nome atual.\n");
    printf("<LEFT>  ou <h> : devolve moeda para o saldo conjunto.\n");
    printf("\n");
    printf(":: Menu de Tópicos ::\n");
    printf("<UP>    ou <k> : aumenta o seu tempo dedicado ao tópico atual.\n");
    printf("<DOWN>  ou <j> : diminui o seu tempo dedicado ao tópico atual.\n");
    printf("<RIGHT> ou <l> : avança para o tópico seguinte.\n");
    printf("<LEFT>  ou <h> : volta para o tópico anterior.\n");
    printf("\n");

    printf("== Controle de Versão ==\n");
    printf("Lembre-se de atualizar o repositório. Antes de iniciar o programa, execute:\n");
    printf("             $ git pull\n");
    printf(":: Adicionando o arquivo de configuração ::\n");
    printf("* Na primeira vez que criar o arquivo gwork.conf, faça:\n");
    printf("             $ git add gwork.conf\n");
    printf(":: Registrando as alterações ::\n");
    printf("* Sempre que modificar os valores dos tópicos ou moedas, execute o commit:\n");
    printf("             $ git cm \"gwork: Nome Completo\"\n");
    printf(":: Enviando as alterações ::\n");
    printf("* Sempre que fizer commit, envie suas modificações para o remoto:\n");
    printf("             $ git push\n");
    printf("\n");

    printf("\nExit status:\n\t0 if ok.\n\t1 some error occurred.\n");
    printf("\nTodo:\n\tLong options not implemented yet.\n");
    printf("\nAuthor:\n\tWritten by %s <%s>\n\n", "Ruben Carlo Benante", "rcb@beco.cc");
    exit(EXIT_FAILURE);
}

/* ---------------------------------------------------------------------- */
/**
 * @ingroup GroupUnique
 * @brief Prints version and copyright information and exit
 * @details Prints version and copyright information (usually called by opt -V)
 * @return Void
 * @author Ruben Carlo Benante
 * @version 20210722.195722
 * @date 2018-05-16
 *
 */
void copyr(void)
{
    IFDEBUG("copyr()");
    printf("%s - Version %s\n", "gwork", VERSION);
    printf("\nCopyright (C) %d %s <%s>, GNU GPL version 2 <http://gnu.org/licenses/gpl.html>. This  is  free  software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law. USE IT AS IT IS. The author takes no responsability to any damage this software may inflige in your data.\n\n", 2023, "Ruben Carlo Benante", "rcb@beco.cc");
    exit(EXIT_FAILURE);
}

/* ---------------------------------------------------------------------- */
/**
 * @ingroup GroupUnique
 * @brief This function initializes some operations before start
 * @details Details to be written in multiple lines
 * @return Void
 * @author Ruben Carlo Benante
 * @version 20210722.195722
 * @date 2021-07-29
 *
 */
/* global initialization function */
data_t gwork_init(wins_t win)
{
    IFDEBUG("gwork_init()");
    data_t da, dc; /* always compare data from AUTHORS and gwork.conf */
    FILE *fa, *fc;
    char line[BIGLINE];
    char *pi, *pf; /* initial and final string position */
    int ast=0; /* states when reading file */
    int hi; /* habil */
    int n; /* general index for students from 0 to ni/ci */
    const char *fauth="AUTHORS";
    const char *fconf="gwork.conf";
    char *sdup, *psep; /* strsep string */
    struct stat sst;

    fwclear0=fwclear1=fwclear2=1;

    if(stat("./.git", &sst) || ! S_ISDIR(sst.st_mode))
        bomb(win, "Sorry, this is not a repository\n", NULL);

    if((fa=fopen(fauth, "r"))==NULL)
        bomb(win, "Sorry, I can't open AUTHORS for reading\n", NULL);

    /* first time, create config file -- or maybe they changed AUTHORS! */
    ast=0; /* title */
    /* This loop reads:
     *      title
     *      coord
     *      name
     *      ni
     */
    n=0; /* zero students */
    da.si=-1;
    while(fgets(line, SBUFF, fa)!=NULL)
    {
        switch(ast)
        {
            case 0: /* title */
                if(strstr(line, "itle: ")!=NULL) /* title: */
                {
                    pi=strchr(line, ':'); /* first */
                    pf=strrchr(line, '='); /* last */
                    if(pi==NULL || pf==NULL || pf<pi+3)
                        bomb(win, "Sorry, AUTHORS format incorrect on title line\n", fa);
                    assert(pf!=NULL);
                    strncpy(da.title, pi+2, pf-pi-3);
                    da.title[pf-pi-3]='\0';
                    /* printf("%s'\n", da.title); */
                    ast=1; /* coordinator */
                }
                break;
            case 1: /* coordinator */
                if(line[0]=='*')
                {
                    pi=strchr(line, ' '); /* first */
                    pf=strrchr(line, '<'); /* last */
                    if(pi==NULL || pf==NULL || pf<pi+3)
                        bomb(win, "Sorry, AUTHORS format incorrect on coordinator line\n", fa);
                    assert(pf!=NULL);
                    strncpy(da.coord, pi+1, pf-pi-2);
                    da.coord[pf-pi-2]='\0';
                    /* printf("%s'\n", da.coord); */
                    ast=2; /* coordinator */
                }
                break;
            case 2: /* students */
            case 3: /* found already one, we are good */
                if(line[0]=='*') /* students */
                {
                    pi=strchr(line, ' '); /* first */
                    pf=strrchr(line, '<'); /* last */
                    if(pi==NULL || pf==NULL || pf<pi+3)
                    {
                        if(n>=1)
                        {
                            ast=4;
                            break;
                        }
                        bomb(win, "Sorry, AUTHORS format incorrect on student line\n", fa);
                    }

                    strncpy(da.name[n], pi+1, pf-pi-2);
                    da.name[n][pf-pi-2]='\0';
                    if(itsme(da.name[n]))
                            da.si=n;
                    n++;
                    if(n>=MAXG)
                        bomb(win, "Sorry, AUTHORS too long\n", fa);
                    ast=3; /* keep reading students til the end */
                    /* printf("%s'\n", da.name[ni-1]); */
                }
            default:
                break;
        }
    }

    fclose(fa); /* AUTHORS on memory */

    /* AUTHORS don't save:
     * group:
     *      da.ni : num students : ok
     *      da.si : current student : ok
     * menu:
     *      da.change : changed data on screen?
     *      da.ucoins : unused coins : COINS
     *      da.coins : coins for everyone : zero
     *      da.habil : habilities : zero
     *      da.bonus : bonus of everyone : zero
     *      da.mitem : start item : zero
     */
    switch(ast)
    {
        case 0:
            bomb(win, "Sorry, AUTHORS format incorrect on title line\n", NULL);
            break;
        case 1:
            bomb(win, "Sorry, AUTHORS format incorrect on coordinator line\n", NULL);
            break;
        case 2:
            bomb(win, "Sorry, AUTHORS format incorrect on student line\n", NULL);
            break;
        case 3:
        case 4:
            break; /* ok */
        default:
            bomb(win, "Sorry, something is wrong\n", NULL);
    }

    da.ni=n; /* number of students in the group */
    if(da.si<0)
    {
        if(!itsme(da.coord))
            bomb(win, "Sorry, you are not in the AUTHORS file and you are not a COORDINATOR\n", NULL);
        da.si=COORD; /* COORD */
        da.cd=COORD;
    }

    da.ucoins=COINS;
    da.change=0;
    for(n=0; n<da.ni; n++)
    {
        da.coins[n]=0; /* authors = zero coins */
        for(hi=0; hi<LASTHABIL; hi++)
        {
            da.habil[n][hi]=8; /* start point half way */
            da.bonus[hi]=0; /* all bonus are zero */
        }
    }
    da.mpes=0; /* select first item (person) */
    da.mhab=0; /* select first item (skills) */
    da.themenu=0; /* 0=names, 1=habils */
    if(da.si>=da.ni)
        bomb(win, "Sorry, I can't determine who you are in the AUTHORS file\n", NULL);

    assert(da.ni!=0);
    /* printf("'author:%s'\n'", da.coord); */
    /* printst(da); */

    /* printf("ni=%d, ast=%d\n", ni, ast); */
/* ---------------------------------------------------------------------- */

    /* open gwork.conf for reading if you can */
    if((fc=fopen(fconf, "r"))!=NULL)
    {
        /* read the config file:
         *      title;coord;ucoins
         *      coins;h0;h1;h2;h3;h4;h5;name1
         *      coins;h0;h1;h2;h3;h4;h5;name2
         *      ...
         */
        ast=0; /* title */
        n=0; /* zero students */
        dc.si=-1; /* achar quem sou */
        while(fgets(line, SBUFF, fc)!=NULL)
        {
            sdup=strdup(line);
            psep=sdup;
            switch(ast)
            {
                case 0: /* title;coordinator;ucoins */
                    strncpy(dc.title, strsep(&psep, ";\n"), SBUFF-1);
                    strncpy(dc.coord, strsep(&psep, ";\n"), SBUFF-1);
                    dc.ucoins=strtol(strsep(&psep, ";\n"), NULL, 10);
                    ast=1; /* students */
                    break;
                case 1: /* coins;h0;h1;h2;h3;h4;h5;name1 */
                    dc.coins[n]=strtol(strsep(&psep, ";\n"), NULL, 10);
                    for(hi=0; hi<LASTHABIL; hi++)
                        dc.habil[n][hi]=strtol(strsep(&psep, ";\n"), NULL, 10);
                    /* ime=*strsep(&psep, ";"); /1* *=me, +=other *1/ */
                    /* if(ime=='*') */
                        /* dc.si=n; */
                    strncpy(dc.name[n], strsep(&psep, ";\n"), SBUFF-1);
                    /* printf("name dc '%s'\n", dc.name[n]); */
                    if(itsme(dc.name[n]))
                        dc.si=n;
                    n++;
                    break;
            }
            if(sdup)
                free(sdup);
            /* printf("DEBUG munmap chunck\n"); */
        }
        fclose(fc);

        dc.ni=n;
        if(dc.si<0)
        {
            if(!itsme(dc.coord))
                bomb(win, "Sorry, you are not in the gwork.conf file and you are not the COORDINATOR\n", NULL);
            dc.si=COORD; /* COORD */
            dc.cd=COORD; /* it is the coordinator */
        }
        if(dc.si>=dc.ni)
            bomb(win, "Sorry, I can't determine who you are in the gwork.conf\n", NULL);

        set_bonus(dc.habil[dc.si], dc.bonus);
        /* dc.ucoins=COINS-0/1* used TODO *1/; */
        dc.mpes=0; /* select first item (person) */
        dc.mhab=0; /* select first item (skills) */
        dc.themenu=0; /* 0=names, 1=habils */
        if(groupcmp(da, dc)) /* AUTHORS == gwork.conf */
        {
            /* printf("return da\n"); */
            /* abort(); */
            return dc;
        }
    }

    /* saveconf(d); /1* creates gwork.conf *1/ */
    /* saving:
     *      title;coord;ucoins
     *      coins;h0;h1;h2;h3;h4;h5;name1
     *      coins;h0;h1;h2;h3;h4;h5;name2
     *      ...
     */
    if(da.cd==COORD || dc.cd==COORD)
    {

        printf("AUTHORS and gwork.conf differ. Save anyway? (type 'yes')\n");
        igr(fgets(line, BIGLINE, stdin));
        if(!strcmp(line, "yes\n"))
            saveconf(win, da);
        else
            bomb(win, "AUTHORS and gwork.conf differ and you are on read-only mode.\n" , NULL);
    }

    /* AUTHORS don't save:
     * group:
     *      da.ni : num students : ok
     *      da.si : current student : ok
     *      name, coord, title : saved
     * menu:
     *      da.ucoins : unused coins : COINS
     *      da.coins : coins for everyone : zero
     *      da.habil : habilities : zero
     *      da.bonus : bonus of everyone : zero
     *      da.mitem : start item : zero
     */
    IFDEBUG("gwork_init() end");
    return da;

}

/* ---------------------------------------------------------------------- */
/* creates gwork.conf */
void saveconf(wins_t win, data_t dt)
{
    IFDEBUG("saveconf");
    FILE *fc;
    char line[BIGLINE];
    int hi; /* habil */
    int n; /* general index for students from 0 to ni/ci */
    const char *fconf="gwork.conf";
    /* char ime; /1* *=me ; +=other *1/ */
    char shab[SMALL], tmp[SBUF2];

    /* saving:
     *      title;coord;ucoins
     *      coins;h0;h1;h2;h3;h4;h5;name1
     *      coins;h0;h1;h2;h3;h4;h5;name2
     *      ...
     */
    if((fc=fopen(fconf, "w"))==NULL)
        bomb(win, "Sorry, I can't create gwork.conf\n", NULL);

    sprintf(line, "%s;%s;%d\n", dt.title, dt.coord, dt.ucoins);
    /* snprintf(line, BIGLINE, "%150s;%150s\n", dt.title, dt.coord); */
    /* printf("DEBUG '%s'\n", line); */
    /* ----------> saving: title;coord;ucoins */
    fputs(line, fc);

    for(n=0; n<dt.ni; n++)
    {
        snprintf(line, BIGLINE, "%d", dt.coins[n]);
        /* printf("reg1:'%s'\n", line); */
        for(hi=0; hi<LASTHABIL; hi++)
        {
            snprintf(shab, SMALL, ";%d", dt.habil[n][hi]);
            strncat(line, shab, SMALL);
        }
        snprintf(tmp, SBUF2, ";%s\n", dt.name[n]);
        strncat(line, tmp, SBUF2);
        /* ----------> saving: coins;h0;h1;h2;h3;h4;h5;name */
        fputs(line, fc);
    }

    fclose(fc); /* gwork.conf */
    IFDEBUG("saveconf end");
}

/* ---------------------------------------------------------------------- */
/* print struct */
void printst(data_t d)
{
    IFDEBUG("printst");
    int i, hi;

    printf("\n\n-----------------------group_t\n");
    printf("d.ni='%d'\n", d.ni);
    printf("d.si='%d'\n", d.si);
    for(i=0; i<d.ni; i++)
        printf("d.name[%d]='%s'\n", i, d.name[i]);
    printf("d.coord='%s'\n", d.coord);
    printf("d.title='%s'\n", d.title);
    printf("-------------------------menu_t\n");
    printf("d.ucoins=%d", d.ucoins);
    for(i=0; i<d.ni; i++)
    {
        printf("\nd.coins[%d]='%d'\n", i, d.coins[i]);
        for(hi=0; hi<LASTHABIL; hi++)
            printf("d.habil[%d][%d]='%d', ", i, hi, d.habil[i][hi]);
        printf("\n");
    }
    for(hi=0; hi<LASTHABIL; hi++)
        printf("d.bonus[%d]='%d', ", hi, d.bonus[hi]);
    printf("\nd.mpes='%d'\n", d.mpes);
    printf("\nd.mhab='%d'\n", d.mhab);
    printf("\nd.themenu='%d'\n", d.themenu);
}

/* ---------------------------------------------------------------------- */
/* compare two group_t structures */
int groupcmp(data_t ga, data_t gb)
{
    IFDEBUG("groupcmp");
    int n;

    /* printst(ga); */
    /* printst(gb); */
    /* abort(); */
    if(ga.ni != gb.ni)
    {
        IFDEBUG("groupcmp end");
        return 0;
    }
    if(ga.si != gb.si)
    {
        IFDEBUG("groupcmp end");
        return 0;
    }
    for(n=0; n<ga.ni; n++)
        if(strncmp(ga.name[n], gb.name[n], SBUFF))
        {
            IFDEBUG("groupcmp end");
            return 0;
        }
    if(strncmp(ga.coord, gb.coord, SBUFF))
    {
        IFDEBUG("groupcmp end");
        return 0;
    }
    if(strncmp(ga.title, gb.title, SBUFF))
    {
        IFDEBUG("groupcmp end");
        return 0;
    }

    IFDEBUG("groupcmp end");
    return 1;
}

/* ---------------------------------------------------------------------- */
/* return true if the name is current user */
int itsme(char *name)
{
    IFDEBUG("itsme");

    uid_t uid;
    struct passwd *pw = NULL;
    char *sysuser, *gecos;
    char sysname[SBUFF]={0};
    char cmpname[SBUFF]={0};
    int i;

    /* get user */
    uid = getuid();
    if((pw = getpwuid(uid))==NULL) /* get user id */
    {
        IFDEBUG("itsme end");
        return 0; /* error, i dont know */
    }
    sysuser=pw->pw_name; /* remember free */
    gecos=pw->pw_gecos; /* gecos finger user information */

    if(sysuser==NULL || gecos==NULL || name==NULL)
        return 0;

    strncpy(sysname, strsep(&gecos, ";,\n"), SBUFF-1);
    for(i=0; sysname[i]; i++)
        sysname[i] = tolower(sysname[i]);
    /* printf("sysname '%s' name '%s'\n", sysname, name); */
    for(i=0; name[i]; i++)
        cmpname[i] = tolower(name[i]);

    /* str case insentitive : tolower both names */
    /* if(strcasecmp(sysname, name)) */
    /* check substring */
    if(strstr(sysname, cmpname)==NULL && strstr(cmpname, sysname)==NULL)
        return 0; /* not substring */

    /* printf("achei!\n\n"); */
    return 1; /* yes, this is the guy */
}

/* ---------------------------------------------------------------------------- */
/* vi: set ai cin et ts=4 sw=4 tw=0 wm=0 fo=croqltn : C config for Vim modeline */
/* Template by Dr. Beco <rcb at beco dot cc>  Version 20160714.153029           */

